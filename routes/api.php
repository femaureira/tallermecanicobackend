<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});



//Controladores del vehículo

Route::resource('/vehicles','VehicleController');

Route::get('/getvehiclebycustomer/{id}', 'VehicleController@getvehiclebycustomer');

Route::get('/getvehiclebytransaction/{id}','VehicleController@getvehiclebytransaction');

Route::get('/getvehiclebystate/{id}','VehicleController@getvehiclebystate');

//Controladores del checkIn

Route::resource('/checkins','CheckInController');

Route::get('/getcheckinbyvehicle/{id}','CheckInController@getcheckinbyvehicle');

//Controladores del customer

Route::resource('/customers','CustomerController');

Route::get('/getcustomerbyvehicle/{id}','CustomerController@getcustomerbyvehicle');

//Controladores del estado

Route::resource('/states','StateController');

Route::get('/getstatebytransaction/{id}','StateController@getstatebytransaction');

//Controladores del service

Route::resource('/services','ServiceController');

Route::get('/getservicebydetail/{id}','ServiceController@getservicebydetail');

Route::get('/getservicebytypeservice/{id}','ServiceController@getservicebytypeservice');

//Controladores del typeservice

Route::resource('/typeservices','TypeServiceController');

//Controladores del replacement

Route::resource('/replacements','ReplacementController');

//Controladores del detailreplacement

Route::resource('/detailreplacements','DetailReplacementController');

//Controladores del detail

Route::resource('/details','DetailController');

Route::get('/getdetailbytransaction/{id}','DetailController@getdetailbytransaction');


//Controladores del transaction

Route::resource('/transaction','TransactionController');

Route::get('/gettotalbytransaction/{id}','TransactionController@gettotalbytransaction');

Route::get('/getdeliverdatebytransaction/{id}','TransactionController@getdeliverdatebytransaction');

Route::post('/maketransaction','TransactionController@maketransaction');

Route::get('/getlistbytransaction/{id}','TransactionController@getlistbytransaction');

Route::get('/getdeliverdateandtotalbylist','TransactionController@getdeliverdateandtotalbylist');

Route::get('/gettotalbydate/{date}','TransactionController@gettotalbydate');

Route::get('/gettransactionbyvehicle/{id}','TransactionController@gettransactionbyvehicle');