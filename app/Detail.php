<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Detail extends Model
{
    protected $fillable=[
        'service_id','transaction_id','quantity','price','total',
    ];

    public function replacements(){
        return $this->belongsToMany('App\Replacement','detail_replacement'); 
    }

}
