<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DetailReplacement extends Model
{
    protected $fillable=[
        'detail_id','replacement_id','quantity','price','total',
    ];
    
}
