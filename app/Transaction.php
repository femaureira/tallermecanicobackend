<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Transaction extends Model
{
    protected $fillable=[
        'state_id','checkin_id','vehicle_id','total','startdate','enddate',
        
    ];

    public function CheckIn(){
        return $this->hasOne('App\CheckIn');
    }

    public function state(){
        return $this->belongsTo('App\State');
    }

    public function vehicle(){
        return $this->belongsTo('App\Vehicle');
    }

    public function services(){
        return $this->belongsToMany('App\Service','details');
    }

    

}
