<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Transaction;
use App\Detail;
use App\DetailReplacement;
use App\Service;
use App\CheckIn;
use App\Replacement;
use App\State;
use DateTime;

class TransactionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return Transaction::all();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input = $request->all();

        Transaction::create($input);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function gettotalbytransaction($id){
        $transaction=Transaction::where('id',$id)->get();
        $total=$transaction[0]->total;
        return $transaction;    
    }

    public function getdeliverdatebytransaction($id){
        $details=Detail::where('transaction_id',$id)->get();
        $hours=0;
        foreach($details as $detail){
            $serviceid=$detail->service_id;
            $quantity=$detail->quantity;
            $service=Service::where('id',$serviceid)->get();
            $timeservice=$service[0]->time;
            $hours=$hours+$quantity*$timeservice;
        }
        return getDate(time()+ ($hours)*3600*24/8 + 24);
    }

    public function maketransaction(Request $request){

        //Se crea el nuevo checkin
        $checkin=new CheckIn;
        $checkin['vehicle_id']=$request['vehicle_id'];
        $check = $request['checkin'];
        $checkin['bloopers']=$check['bloopers'];
        $checkin['requirements']=$check['requirements'];
        $checkin->save();       
        
        //Se crea la nueva transaccion
        $transaction =new Transaction;
        $transaction['state_id']=1;
        $transaction['vehicle_id']=$request['vehicle_id'];
        $transaction['checkin_id']=$checkin['id'];
        $transaction['total']=0;
        $transaction['startdate']=date('y-m-d');
        $transaction['enddate']=date('y-m-d');
        $transaction->save();

        //se crean los detalles
        $services=$request->services;
        foreach($services as $service){
            $detail=new Detail;
            $servicedb=Service::where('id',$service['service_id'])->get();
            
            $detail['transaction_id']=$transaction['id'];
            $detail['service_id']=$service['service_id'];
            $detail['price']=$servicedb[0]['price'];
            $detail['quantity']=$service['quantity'];
            $detail['total']=$service['total'];
            $detail->save();

            $replacements=$service['replacements'];
            foreach($replacements as $replacement){
                $detailreplacement=new DetailReplacement;
                $detailreplacement['detail_id']=$detail['id'];
                $detailreplacement['replacement_id']=$replacement['replacement_id'];
                $detailreplacement['quantity']=$replacement['quantity'];
                $detailreplacement['price']=$replacement['price'];
                $detailreplacement['total']=$replacement['quantity']*$replacement['price'];
                $detailreplacement->save();
            }

        }

        $details=Detail::where('transaction_id',$transaction['id'])->get();
        $hours=0;
        $total=0;
        foreach($details as $detail){
            //calcula la fecha de entrega            
            $serviceid=$detail['service_id'];
            $quantity=$detail['quantity'];
            $service=Service::where('id',$serviceid)->get();
            $timeservice=$service[0]->time;
            $hours=$hours+$quantity*$timeservice;
            
            //calcula el total
            $total=$total+$detail['total'];
        }


        $transaction['total']=$total;
        $transaction['enddate']= date('y-m-d', time()+ $hours * 3600 * 24 / 8 + 24*3600);
        $transaction->save();

        return $transaction;
    }


    public function getlistbytransaction($id){
        $details=Detail::where('transaction_id',$id)->get();
        $list=[];
        foreach($details as $detail){
            $service=Service::where('id',$detail->service_id)->get();
            $object=[
                'name'=>$service[0]->name,
                'type'=>'Servicio',
                'quantity'=>$detail->quantity,
                'price'=>$detail->price,
                'total'=>($detail->quantity)*($detail->price),
            ];
            $list[]=$object;
            $detailreplacements=DetailReplacement::where('detail_id',$detail->id)->get();
            foreach($detailreplacements as $detailreplacement){
                $replacement=Replacement::where('id',$detailreplacement->replacement_id)->get();
                $object=[
                    'name'=>$replacement[0]->name,
                    'type'=>'Repuesto/Producto',
                    'quantity'=>$detailreplacement->quantity,
                    'price'=>$detailreplacement->price,
                    'total'=>$detailreplacement->total,
                ];
                $list[]=$object;
            }
        }
        return $list;
    }

    public function getdeliverdateandtotalbylist(Request $request){ 
        $services=$request->services;
        $time=0;
        $total=0;


        foreach($services as $service){
            $servicio = Service::where('id',$service['service_id'])->get();
            $time=$time+($servicio[0]->time)*($service['quantity']);
            $total=$total+($service['price'])*($service['quantity']);
            $replacements=$service['replacements'];
            foreach($replacements as $replacement){
                $total=$total+($replacement['quantity'])*($replacement['price']);
            }
        } 
        $deliverdate=date('y-m-d', time()+ $time * 3600 * 24 / 8 + 24*3600);
        return [
            'deliverdate'=>$deliverdate,
            'total'=>$total,
        ];
    }

    public function gettotalbydate($date){
        $transactions=Transaction::whereDate('created_at',$date)->get();
        $total=0;
        foreach($transactions as $transaction){
            $total=$total+$transaction->total;
        }      
        return $total;
    }

    public function gettransactionbyvehicle($id){
        return Transaction::where('vehicle_id',$id)->get()->load(['state']);
    }


}
