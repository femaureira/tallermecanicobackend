<?php

namespace App\Http\Controllers;

use App\Transaction;
use Illuminate\Http\Request;
use App\Vehicle;


class VehicleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return Vehicle::all();        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input = $request->all();
        Vehicle::create($input);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $input = Vehicle::findOrFail($id);
    
        $input->customer_id = $request->customer_id;
        $input->patent = $request->patent;
        $input->brand = $request->brand;
        $input->model = $request->model;
        $input->kilometers = $request->kilometers;
        $input->year = $request->year;
        $input->bodywork = $request->bodywork;
        $input->color = $request->color;
        $input->description= $request->description;
        $input->save();
    
        return response()->json($input,201); 
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function getvehiclebycustomer($id){
        $vehicles=Vehicle::where('customer_id', $id)->get();
        return $vehicles;
    }

    public function getvehiclebytransaction($id){
        $transaction=Transaction::where('id',$id)->get();
        $vehicleid=$transaction[0]->vehicle_id;
        return Vehicle::where('id',$vehicleid)->get();
    }

    public function getvehiclebystate($id){
        $transactions=Transaction::where('state_id',$id)->get();
        $vehicles=[];
        foreach($transactions as $transaction){
            $vehicle=Vehicle::where('id',$transaction->vehicle_id)->get();
            $vehicles[sizeof($vehicles)]=$vehicle[0];
        }
        $vehicles=array_unique($vehicles);
        return $vehicles;
    }
}
