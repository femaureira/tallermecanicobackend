<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Customer;
use App\Vehicle;

class CustomerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return Customer::all();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input=$request->all();

        Customer::create($input);

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $input = Customer::findOrFail($id);
    
        $input->name = $request->name;
        $input->direction = $request->direction;
        $input->email = $request->email;
        $input->phone=$request->phone;
        $input->bornDate=$request->bornDate;
        $input->rut=$request->rut;

        $input->save();
    
        return response()->json($input,201); 
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function getcustomerbyvehicle($id){
        $vehicle=Vehicle::where('id',$id)->get();
        $customerid=$vehicle[0]->customer_id;
        $customer = Customer::where('id',$customerid)->get();
        return $customer[0];
    }
    
}
