<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CheckIn extends Model
{
    protected $fillable=[
        'vehicle_id','bloopers','requirements',
    ];

    public function vehicle(){
        return $this->belongsTo('App\Vehicle');
    }

    public function transaction(){
        return $this->hasOne('App\Transaction');
    }

    

}
