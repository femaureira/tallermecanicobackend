<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TypeService extends Model
{
    protected $fillable=[
        'name','description',
    ];

    public function services(){
        return $this->belongsTo('App\Service');
    }
}
