<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Service extends Model
{
    protected $fillable=[
        'typeservices_id','name','price','time',
    ];

    public function transactions(){
        return $this->belongsToMany('App\Transaction','details');
    }

    public function typeservices(){
        return $this->hasMany('App\TypeService');
    }

}   
