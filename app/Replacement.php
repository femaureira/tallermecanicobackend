<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Replacement extends Model
{
    protected $fillable=[
        'name','brand','price',
    ];
    
    public function details(){
        return $this->belongsToMany('App\Detail','detail_replacement');
    }

    
}
