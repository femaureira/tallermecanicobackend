<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class State extends Model
{
    protected $fillable=[
        'name','description',
    ];
    public function transactions(){
        return $this->belongsToMany('App\Transaction');
    }
}
