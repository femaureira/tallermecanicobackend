<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Vehicle extends Model
{

    protected $fillable=[
        'customer_id','patent','brand','model','kilometers','year','bodywork','color','description',
    ];


    public function customer(){
        return $this->belongsTo('App\Customer');
    }

    public function vehicles(){
        return $this->hasMany('App\CheckIn');
    }

    public function transactions(){
        return $this->hasMany('App\Transaction');
    }
}
