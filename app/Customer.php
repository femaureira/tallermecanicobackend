<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Customer extends Model
{
    
    protected $fillable=[
        'name','direction','email','phone','bornDate','rut',
    ];
    
    public function vehicles(){
        return $this->hasMany('App\Vehicle');
    }

}
